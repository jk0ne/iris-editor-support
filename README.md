# IRIS Syntax support

This repository contains language support for multiple editors including
vim, sublime text and VS-Code. 

## Features

- **Syntax Highlighting**: Enjoy a more readable and navigable coding
  experience with syntax highlighting tailored for IRIS.
- **Code Snippets**: Speed up your development with a library of useful code
  snippets for common pipeline patterns and tasks.
- **Easy Integration**: Seamlessly integrates with your existing setup,
  enhancing your workflow without any additional overhead.

## Vim

To activate IRIS support in vim, include the contents of the `vim/` 
folder here into your vim configuration.

## VSCode

The Visual Studio Code extension provides comprehensive language support for
IRIS, a domain-specific language designed for building robust data pipelines.
It includes syntax highlighting, code snippets, and other editing features to
enhance your IRIS development experience.

## Installation

1. Open Visual Studio Code.
2. Go to the Extensions view by clicking on the square icon on the sidebar or pressing `Ctrl+Shift+X`.
3. Search for `IRIS - Pipeline language`.
4. Click on the `Install` button.

## Usage

Once installed, the extension automatically applies syntax highlighting to
files recognized as IRIS pipelines. To use a snippet, simply start typing the
keyword in an IRIS file, and the suggestion box will present the relevant
snippets.

## Contributing

Contributions to this extension are welcome. If you'd like to improve the IRIS
syntax support or expand the snippet library, please feel free to submit a pull
request.

## License

This extension is released under the [LGPL v2.1 License](LICENSE).




