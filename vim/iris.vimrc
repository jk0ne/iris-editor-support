au BufNewFile,BufRead *.pipeline setf IRIS

fun! s:SelectIRIS()
      if getline(1) =~# '<!--IRIS'
          set ft=IRIS
      endif
endfun

au BufNewFile,BufRead * call s:SelectIRIS()

