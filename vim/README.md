# IRIS support for Vim

## Installing

To activate the vim syntax highlighting for IRIS pipelines, place the 
`IRIS-syntax.vim` in your vim configuration's syntax/ folder.

Then to make it activate when editing IRIS pipelines, include the
contents if `iris.vimrc` into your vimrc.

## DTL is required

Since IRIS pipelines make use of DTL expressions, you'll need to 
install the DTL vim syntax files from the DTL repository. You can
find them [here](https://gitlab.com/jk0ne/DTL/-/tree/master/tools/editors/vim?ref_type=heads)
